
.PHONY: install
install: get_gpg_keys
	python3 -m venv .venv
	.venv/bin/python3 -m pip install -r requirements.txt

.PHONY: get_gpg_keys
get_gpg_keys:
	gpg --locate-keys torvalds@kernel.org gregkh@kernel.org

.PHONY: install-cron
install-cron:
	install --compare --mode=0755 --owner=root --verbose cron /etc/cron.daily/dkuu
