# Debian Kernel Update Utility

A tool to check kernel.org for the latest kernel versions and locally build a new one if available.

## Usage

1. Clone this repo

       git clone https://gitlab.com/ixydo/dkuu.git

2. Install dependencies

       make install

2. Build a new kernel

       ./dkuu [workdir]

   `workdir` will default to $HOME/src if omitted

3. Optionally, install the cron

       make install-cron

## Overview

The tool checks kernel.org for the latest kernels.  The logic is currently hard-coded as follows:

- No major upgrades, i.e. kernel 6.x won't be installed if you're running 5.x
- Minor upgrades will be built if the micro version is at least '3'.  For
  example 5.7.2 won't be build if you're on 5.6.x, but when 5.7.3 is released
  it will be built and installed.
- Otherwise new micro versions will be built and installed

When building a new kernel the script sources the currently running kernel
config from `/boot/config.gz` and uses that as the basis to run `make
olddefconfig`, i.e. accepting any new default settings where one wasn't present
before.

Downloads are verified against their GPG signature, though trust of the signer
keys is left to the user to set.

### cron script

The cron script will save all it's output to
`/var/log/_etc_cron.daily_dkuu_$LOGDATE.{err,log}`.  If it encounters an error
during execution it will send those logs to the system logger with facilty
`cron` (`clock daemon`) and severity `err`.

After a successful build the script will install the new kernel and headers.

Finally the script will remove old kernel build files, leaving the generated
`*.deb` alone.  It will also uninstall old kernels, keeping the currently
running kernel, newly built kernel and previously built kernel.

## ToDo

- Add tests
